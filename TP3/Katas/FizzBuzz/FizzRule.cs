﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.FizzBuzz
{
    public class FizzRule: IRules
    {
        private static readonly int NUMBER_FIZZ_MULTIPLE = 3;
        public bool Matches(int number)
        {
            return number % NUMBER_FIZZ_MULTIPLE == 0;
        }

        public string GetReplacement()
        {
            return "Fizz";
        }
    }
}
