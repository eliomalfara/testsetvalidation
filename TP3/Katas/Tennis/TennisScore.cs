﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.Tennis
{
    public class TennisScore
    {
        public int Number { get; }

        public TennisScore(int number)
        {
            if (number < 0)
            {
                throw new ArgumentOutOfRangeException("Le nombre choisi doit etre un nombre entier positif");
            }
            this.Number = number;
        }
    }
}
