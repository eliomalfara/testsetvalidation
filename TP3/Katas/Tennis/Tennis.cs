﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.Tennis
{

    public class Tennis
    {
        private static readonly int INDEX_DEUCE = 3;
        private static readonly int NUMBER_POINT_FOR_FORTY = 3;
        private static string[] equalScore = new string[] { "Love-All", "Fifteen-All", "Thirty-All", "Deuce" };
        private static string[] classicScore = new string[] { "Love", "Fifteen", "Thirty", "Forty" };
        public static string GetScore(TennisScore player1Points, TennisScore player2Points)
        {
            string result = "";
            if (CheckScoreEqual(player1Points, player2Points))
            {
                result = GetScoreEqual(player1Points, player2Points);
            }
            else if (OnePlayerHas4PointsMinimum(player1Points, player2Points))
            {
                result = CheckPointDifference(player1Points, player2Points);
            }
            else
            {
                result = GetClassicScore(player1Points, player2Points);
            }
            return result;
        }

        private static string CheckPointDifference(TennisScore player1Points, TennisScore player2Points)
        {
            string result = "";
    
            if (CheckScore1PointDifference(player1Points, player2Points))
            {
                result = GetScore1PointDifference(player1Points, player2Points);
            }
            else
            {
                result = GetScore2PointsOrMoreDifference(player1Points, player2Points);
            }

            return result;
        }

        private static bool OnePlayerHas4PointsMinimum(TennisScore player1Points, TennisScore player2Points)
        {
            return player1Points.Number >= 4 || player2Points.Number >= 4;
        }

        private static bool CheckScoreEqual(TennisScore player1Points, TennisScore player2Points)
        {
            return PointDifference(player1Points, player2Points) == 0;
        }

        private static bool CheckScore1PointDifference(TennisScore player1Points, TennisScore player2Points)
        {
            return Math.Abs(PointDifference(player1Points, player2Points)) == 1;
        }
        
        private static int PointDifference(TennisScore player1Points, TennisScore player2Points)
        {
            return player1Points.Number - player2Points.Number;
        }

        private static string GetScoreEqual(TennisScore player1Points, TennisScore player2Points)
        {
            return player2Points.Number > NUMBER_POINT_FOR_FORTY ? equalScore[INDEX_DEUCE] : equalScore[player1Points.Number];
        }


        private static string GetScore1PointDifference(TennisScore player1Points, TennisScore player2Points)
        {
            return PointDifference(player1Points, player2Points) == 1 ? "Advantage Player1" : "Advantage Player2";
        }

        private static string GetScore2PointsOrMoreDifference(TennisScore player1Points, TennisScore player2Points)
        {
            return PointDifference(player1Points, player2Points) >= 2 ? "Win Player1" : "Win Player2";
        }
        
        private static string GetClassicScore(TennisScore player1Points, TennisScore player2Points)
        {
            return classicScore[player1Points.Number] + "-" + classicScore[player2Points.Number];
        }
    }
}
