﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Katas.Bowling
{
    public class Roll
    {
        public int Value { get; }

        public Roll(int value)
        {
            if(value < 0 || value > 10)
            {
                throw new ArgumentException("La valeur du lancé est invalide");
            }

            Value = value;
        }

        public static implicit operator int(Roll r) => r.Value;
        public static implicit operator Roll(int v)
        {
            Roll r = new Roll(v);
            return r;
        }
    }
}
