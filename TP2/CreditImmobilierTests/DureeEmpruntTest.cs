﻿using CreditImmobilier;
using System;
using Xunit;

namespace CreditImmobilierTests
{
    public class DureeEmpruntTest
    {
        private static readonly int DUREE_MIN = 9;
        private static readonly int DUREE_MAX = 25;

        [Fact]
        public void TestConstructeurErreurInferieur()
        {
            ArgumentException exception = Assert.Throws<ArgumentException>(() => new DureeEmprunt(8));
            Assert.Equal($"Temps de durée de l'emprunt < {DUREE_MIN} ans", exception.Message);
        }

        [Fact]
        public void TestConstructeurErreurMaximum()
        {
            ArgumentException exception = Assert.Throws<ArgumentException>(() => new DureeEmprunt(26));
            Assert.Equal($"Temps de durée de l'emprunt > {DUREE_MAX} ans", exception.Message);
        }

        [Theory]
        [InlineData(9)]
        [InlineData(25)]
        public void TestConstructeurOK(int dureeChoisi)
        {
            DureeEmprunt duree = new DureeEmprunt(dureeChoisi);
            Assert.Equal(dureeChoisi, duree.valeur);
        }
    }
}
