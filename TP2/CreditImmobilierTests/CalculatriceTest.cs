using CreditImmobilier;
using System;
using Xunit;

namespace CreditImmobilierTests
{
    public class CalculatriceTest
    {
        private static readonly bool[] TABLEAU_VALEURS_CHECKBOX = { false, true, true, true, false };
        private static readonly CapitalEmprunte CAPITAL = new CapitalEmprunte(175000);
        private static readonly DureeEmprunt DUREE = new DureeEmprunt(25);
        private static readonly TauxAnnuel TAUX_ANNUEL = new TauxAnnuel(TypeTaux.BON_TAUX, DUREE);
        private static readonly TauxAssurance TAUX_ASSURANCE = new TauxAssurance(TauxAssurance.CalculTauxAssuranceDeduit(TABLEAU_VALEURS_CHECKBOX));
        private static readonly Calculatrice CALCULATRICE = new Calculatrice(CAPITAL, DUREE, TAUX_ANNUEL, TAUX_ASSURANCE);

        [Fact]
        public void TestCalculerMensualite()
        {
            Assert.Equal(681, CALCULATRICE.CalculerMensualite());
        }

        [Fact]
        public void TestCalculerMensualiteGlobal()
        {
            Assert.Equal(783, CALCULATRICE.CalculerMensualiteGlobal());
        }

        [Fact]
        public void TestCalculerMensualiteAssurance()
        {
            Assert.Equal(102, CALCULATRICE.CalculerMensualiteAssurance());
        }

        [Fact]
        public void TestCalculerInteretsTotaux()
        {
            Assert.Equal(29300, CALCULATRICE.CalculerInteretsTotaux());
        }

        [Fact]
        public void TestCalculerAssuranceTotal()
        {
            Assert.Equal(30625, CALCULATRICE.CalculerAssuranceTotal());
        }

        [Fact]
        public void TestCalculerCapital()
        {
            Assert.Equal(81720, CALCULATRICE.CalculerCapital(10));
        }
    }
}