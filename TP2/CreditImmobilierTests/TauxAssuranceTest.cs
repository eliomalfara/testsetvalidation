﻿using CreditImmobilier;
using System;
using Xunit;

namespace CreditImmobilierTests
{
    public class TauxAssuranceTest
    {
        [Theory]
        [InlineData(0.3, 0)]
        [InlineData(0.25, -0.05)]
        [InlineData(0.6, 0.3)]
        public void TestConstructeurOK(double tauxAttendu, double tauxDeduit)
        {
            TauxAssurance tauxAssurance = new TauxAssurance(tauxDeduit);
            Assert.Equal(tauxAttendu, tauxAssurance.valeur, 2);
        }

        [Fact]
        public void TestCalculTauxAssuranceDeduit()
        {
            bool[] tableauCheckBox = { false, true, true, true, false };
            Assert.Equal(0.4, TauxAssurance.CalculTauxAssuranceDeduit(tableauCheckBox), 2);
        }
    }
}
