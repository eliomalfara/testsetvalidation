using CreditImmobilier;
using System;
using Xunit;

namespace CreditImmobilierTests
{
    public class CapitalEmprunteTest
    {
        [Fact]
        public void TestConstructeurErreur()
        {
            ArgumentException exception = Assert.Throws<ArgumentException>(() => new CapitalEmprunte(49999));
            Assert.Equal("Valeur du capital emprunt� < 50.000", exception.Message);
        }

        [Theory]
        [InlineData(50000)]
        [InlineData(int.MaxValue)]
        public void TestConstructeurOK(int capitalChoisi)
        {
            CapitalEmprunte capital = new CapitalEmprunte(capitalChoisi);
            Assert.Equal(capitalChoisi, capital.valeur);
        }
    }
}