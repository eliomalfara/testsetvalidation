﻿using CreditImmobilier;
using System;
using System.Collections.Generic;
using Xunit;

namespace CreditImmobilierTests
{
    public class TauxAnnuelTest
    {
        [Theory]
        [MemberData(nameof(DonneesTestTauxAnnuel))]
        public void TestConstructeurOK(double tauxAttendu, TypeTaux tauxChoisi, DureeEmprunt dureeChoisi)
        {
            TauxAnnuel tauxAssurance = new TauxAnnuel(tauxChoisi, dureeChoisi);
            Assert.Equal(tauxAttendu, tauxAssurance.valeur, 2);
        }

        public static IEnumerable<object[]> DonneesTestTauxAnnuel =>
        new List<object[]>
        {
                new object[] { 0.67, TypeTaux.BON_TAUX, new DureeEmprunt(12) },
                new object[] { 0.73, TypeTaux.TRES_BON_TAUX, new DureeEmprunt(17) },
                new object[] { 0.91, TypeTaux.TRES_BON_TAUX, new DureeEmprunt(22) },
                new object[] { 0.89, TypeTaux.EXCELLENT_TAUX, new DureeEmprunt(25) },
        };
    }
}
