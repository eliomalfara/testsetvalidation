﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmobilier
{
    public struct CapitalEmprunte
    {
        private static readonly int CAPITAL_MIN = 50000;
        public int valeur { get; }
        public CapitalEmprunte(int valeurChoisie)
        {
            if(valeurChoisie < CAPITAL_MIN)
            {
                throw new ArgumentException("Valeur du capital emprunté < 50.000");
            }
            valeur = valeurChoisie;
        }
    }
}
