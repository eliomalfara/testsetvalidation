﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmobilier
{
    public class Calculatrice
    {
        private static readonly double NOMBRE_DE_MOIS_PAR_AN = 12.0;
        private static readonly double CENT = 100.0;

        private CapitalEmprunte capital;
        private DureeEmprunt duree;
        private TauxAnnuel tauxAnnuel;
        private TauxAssurance tauxAssurance;

        private double numerateur;
        private double denominateur;
        private double assurance;
        public Calculatrice(CapitalEmprunte capitalChoisi, DureeEmprunt dureeChoisie, TauxAnnuel tauxAnnuelChoisi, TauxAssurance tauxAssuranceChoisie)
        {
            capital = capitalChoisi;
            duree = dureeChoisie;
            tauxAnnuel = tauxAnnuelChoisi;
            tauxAssurance = tauxAssuranceChoisie;
            numerateur = 0.0;
            denominateur = 0.0;
            assurance = 0.0;
            CalculerValeursInitiales();
        }
        private void CalculerValeursInitiales()
        {
            numerateur = (capital.valeur * ((tauxAnnuel.valeur / CENT) / NOMBRE_DE_MOIS_PAR_AN));
            denominateur = 1 - Math.Pow(1.0 + ((tauxAnnuel.valeur / CENT) / NOMBRE_DE_MOIS_PAR_AN), -duree.valeur * NOMBRE_DE_MOIS_PAR_AN);
            assurance = capital.valeur * ((tauxAssurance.valeur / CENT) / NOMBRE_DE_MOIS_PAR_AN);
        }

        public double CalculerMensualite()
        {
            return Math.Round(numerateur / denominateur);
        }

        public double CalculerMensualiteGlobal()
        {
            return Math.Round((numerateur / denominateur) + assurance);
        }

        public double CalculerMensualiteAssurance()
        {
            return Math.Round(assurance);
        }

        public double CalculerInteretsTotaux()
        {
            return Math.Round(((duree.valeur * NOMBRE_DE_MOIS_PAR_AN) * CalculerMensualite()) - capital.valeur);
        }

        public double CalculerAssuranceTotal()
        {
            return Math.Round(assurance * (duree.valeur * NOMBRE_DE_MOIS_PAR_AN));
        }

        public double CalculerCapital(double nombreAnnee)
        {
            return Math.Round(CalculerMensualite() * NOMBRE_DE_MOIS_PAR_AN * nombreAnnee);
        }
    }
}
