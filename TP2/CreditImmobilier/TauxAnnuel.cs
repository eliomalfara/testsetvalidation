﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmobilier
{
    public class TauxAnnuel
    {
        private static readonly int LIMITE_10_ANS = 12;
        private static readonly int LIMITE_25_ANS = 25;
        private static readonly int ECART_ENTRE_DEUX_LIMITES = 5;

        private static double[,] tableauTaux = new double[3, 4] { { 0.67, 0.85, 1.04, 1.27 }, { 0.55, 0.73, 0.91, 1.15 }, { 0.45, 0.58, 0.73, 0.89 } };
        public double valeur { get; } = 0;
        public TauxAnnuel(TypeTaux tauxChoisi, DureeEmprunt duree)
        {
            valeur = CalculerTauxEnFonctionDesCriteresEtDuType(tauxChoisi, duree);
        }

        private static double CalculerTauxEnFonctionDesCriteresEtDuType(TypeTaux taux, DureeEmprunt duree)
        {
            int ligne = ChoisirLigneTableauTaux(taux);
            int colonne = ChoisirColonneTableauTaux(duree);

            return tableauTaux[ligne, colonne];
        }

        private static int ChoisirLigneTableauTaux(TypeTaux taux)
        {
            return (int)taux;
        }
        private static int ChoisirColonneTableauTaux(DureeEmprunt duree)
        {
            int numeroColonne = 0;
            for (int i = LIMITE_10_ANS; i < LIMITE_25_ANS; i += ECART_ENTRE_DEUX_LIMITES)
            {
                if (duree.valeur <= i)
                {
                    return numeroColonne;
                }
                numeroColonne++;
            }
            return numeroColonne;
        }
    }
}
