﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmobilier
{
    public struct TauxAssurance
    {
        private static readonly double TAUX_ASSURANCE_SPORTIF = -0.05;
        private static readonly double TAUX_ASSURANCE_FUMEUR = 0.15;
        private static readonly double TAUX_ASSURANCE_CARDIAQUE = 0.3;
        private static readonly double TAUX_ASSURANCE_INFORMATIQUE = -0.05;
        private static readonly double TAUX_ASSURANCE_PILOTE = 0.15;

        private static readonly double[] TABLEAU_TAUX = { TAUX_ASSURANCE_SPORTIF, TAUX_ASSURANCE_FUMEUR, TAUX_ASSURANCE_CARDIAQUE, TAUX_ASSURANCE_INFORMATIQUE, TAUX_ASSURANCE_PILOTE };
        public double valeur { get; set; } = 0.3;

        public TauxAssurance(double tauxDeduit)
        {
            valeur += tauxDeduit;
        }

        public static double CalculTauxAssuranceDeduit(bool[] tableauCheckBox)
        {
            double tauxDeduit = 0.0;
            for(int i = 0; i < tableauCheckBox.Length; i++)
            {
                if(tableauCheckBox[i])
                {
                    tauxDeduit += TABLEAU_TAUX[i];
                }
            }
            return tauxDeduit;
        }
    }
}
