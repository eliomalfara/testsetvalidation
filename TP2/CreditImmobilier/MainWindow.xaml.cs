﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CreditImmobilier
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static readonly double NOMBRE_ANNEE_CAPITAL = 10.0;
        private static readonly bool[] TABLEAU_VALEURS_CHECKBOX = {false, false, false, false, false};
        public MainWindow()
        {
            InitializeComponent();
            ComboBoxTaux.ItemsSource = Enum.GetValues(typeof(TypeTaux));
            ComboBoxTaux.SelectedIndex = 0;
        }

        private void ButtonCalcul_Click(object sender, RoutedEventArgs e)
        {
            RemplirTableauCheckBox();

            CapitalEmprunte capital = new CapitalEmprunte(int.Parse(TextBoxCapital.Text));
            DureeEmprunt duree = new DureeEmprunt(int.Parse(TextBoxDuree.Text));
            TauxAnnuel tauxAnnuel = new TauxAnnuel((TypeTaux)ComboBoxTaux.SelectedItem, duree);
            TauxAssurance tauxAssurance = new TauxAssurance(TauxAssurance.CalculTauxAssuranceDeduit(TABLEAU_VALEURS_CHECKBOX));

            Calculatrice calculatrice = new Calculatrice(capital, duree, tauxAnnuel, tauxAssurance);

            AfficherResultats(calculatrice);

        }

        private void RemplirTableauCheckBox()
        {
            if(CheckBoxSportif.IsChecked == true)
            {
                TABLEAU_VALEURS_CHECKBOX[0] = true;
            }
            if(CheckBoxFumeur.IsChecked == true)
            {
                TABLEAU_VALEURS_CHECKBOX[1] = true;
            }
            if (CheckBoxCardiaque.IsChecked == true)
            {
                TABLEAU_VALEURS_CHECKBOX[2] = true;
            }
            if (CheckBoxInformatique.IsChecked == true)
            {
                TABLEAU_VALEURS_CHECKBOX[3] = true;
            }
            if (CheckBoxPilote.IsChecked == true)
            {
                TABLEAU_VALEURS_CHECKBOX[4] = true;
            }
        }

        private void AfficherResultats(Calculatrice calculatrice)
        {
            AfficherResultatMensualiteGlobal(calculatrice.CalculerMensualiteGlobal());
            AfficherResultatMensualiteAssurance(calculatrice.CalculerMensualiteAssurance());
            AfficherResultatInteretsTotaux(calculatrice.CalculerInteretsTotaux());
            AfficherResultatAssuranceTotal(calculatrice.CalculerAssuranceTotal());
            AfficherResultatCapitalDixAns(calculatrice.CalculerCapital(NOMBRE_ANNEE_CAPITAL));
        }

        private void AfficherResultatMensualiteGlobal(double mensualiteGlobal)
        {
            TextBlockResultatMensualiteGlobal.Text = mensualiteGlobal.ToString();
        }

        private void AfficherResultatMensualiteAssurance(double mensualiteAssurance)
        {
            TextBlockResultatMensualiteAssurance.Text = mensualiteAssurance.ToString();
        }

        private void AfficherResultatInteretsTotaux(double interetsTotaux)
        {
            TextBlockIResultatInteretsTotaux.Text = interetsTotaux.ToString();
        }

        private void AfficherResultatAssuranceTotal(double assuranceTotal)
        {
            TextBlockResultatAssuranceTotal.Text = assuranceTotal.ToString();
        }

        private void AfficherResultatCapitalDixAns(double capitalDixAns)
        {
            TextBlockResultatCapitalDixAns.Text = capitalDixAns.ToString();
        }

        private void TextBoxNumber_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
