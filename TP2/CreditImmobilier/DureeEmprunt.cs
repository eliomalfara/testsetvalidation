﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmobilier
{
    public struct DureeEmprunt
    {
        private static readonly int DUREE_MIN = 9;
        private static readonly int DUREE_MAX = 25;
        public int valeur { get; }
        public DureeEmprunt(int valeurChoisie)
        {
            if (valeurChoisie < DUREE_MIN)
            {
                throw new ArgumentException($"Temps de durée de l'emprunt < {DUREE_MIN} ans");
            }
            else if (valeurChoisie > DUREE_MAX)
            {
                throw new ArgumentException($"Temps de durée de l'emprunt > {DUREE_MAX} ans");
            }
            valeur = valeurChoisie;
        }
    }
}
